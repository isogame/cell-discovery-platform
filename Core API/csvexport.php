<?php
//MySQL接続
require_once("../database_connector.php");
$db_session = dbconnect();

$mcc=esch(escs($db_session,$_GET["mcc"]));
$mnc=esch(escs($db_session,$_GET["mnc"]));

$before=esch(escs($db_session,$_GET["before"]));
$after=esch(escs($db_session,$_GET["after"]));

$cellid_before=esch(escs($db_session,$_GET["cellid_before"]));
$cellid_after=esch(escs($db_session,$_GET["cellid_after"]));

$header=esch(escs($db_session,$_GET["header"]));
$debug=esch(escs($db_session,$_GET["debug"]));
$gzip=esch(escs($db_session,$_GET["gzip"]));
$gzipfilename=esch(escs($db_session,$_GET["gzipfilename"]));
$mlscompatible=esch(escs($db_session,$_GET["mlscompatible"]));

$changedextonly=esch(escs($db_session,$_GET["changedextonly"]));

$content = "";
$gzipcontent = "";

if ($gzip == "on") {
    if ($gzipfilename == "") { $gzipfilename = "cdp-distribute-collection-export.csv.gz"; }
    ini_set('zlib.output_compression','Off');
    header('Content-Type: application/gzip');
    header('Content-Disposition: attachment; filename="'.$gzipfilename.'"');
    header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
    header('Pragma: no-cache');
} else {
    header('Content-Type: text/plain');
}

if ($debug == "on") {
    echo "Welcome to Cell Discovery Platform.".PHP_EOL;
    echo "Debug mode is on. This output is incorrect as CSV data. This is a debugging mode for developers.".PHP_EOL;
    echo "Reuqest Parameter List:".PHP_EOL;
    echo "debug=".$debug.PHP_EOL;
    echo "mcc=".$mcc.PHP_EOL;
    echo "mnc=".$mnc.PHP_EOL;
    echo "before=".$before.PHP_EOL;
    echo "after=".$after.PHP_EOL;
    echo "cellid_before=".$cellid_before.PHP_EOL;
    echo "cellid_after=".$cellid_after.PHP_EOL;
    echo "gzip=".$gzip.PHP_EOL;
    echo "mlscompatible=".$mlscompatible.PHP_EOL;
    echo "changedextonly=".$changedextonly.PHP_EOL;
    echo PHP_EOL;
    echo "Run SQL Query: ";
}

//基本形
$sqlquery = "SELECT * from DistributeCollection where 1=1";

//MCCが指定されている場合
if ($mcc !== "") {
    $sqlquery .= " and mcc = '".$mcc."'";
}

//MNCが指定されている場合
if ($mnc !== "") {
    $sqlquery .= " and net = '".$mnc."'";
}

//afterが指定されている場合
if ($after !== "") {
    $sqlquery .= " and updated >= '".$after."'";
}

//beforeが指定されている場合
if ($before !== "") {
    $sqlquery .= " and updated <= '".$before."'";
}

//cellid_afterが指定されている場合
if ($cellid_after !== "") {
    $sqlquery .= " and CONVERT(cell, UNSIGNED) >= '".$cellid_after."'";
}

//cellid_beforeが指定されている場合
if ($cellid_before !== "") {
    $sqlquery .= " and CONVERT(cell, UNSIGNED) <= '".$cellid_before."'";
}

//cchangedextonlyが指定されている場合
if ($changedextonly == "on") {
    $sqlquery .= " and changedext = '1'";
}

if ($debug == "on") {
    echo $sqlquery.PHP_EOL;
}

unset($db_data); 
if ($result = $db_session->query($sqlquery)) {         
    while ($row = $result->fetch_assoc()) {
    $db_data[] = $row;
    }
    $result->free();
}

$count = count($db_data);
if ($debug == "on") {
    echo "Query Data Count: ".$count.PHP_EOL.PHP_EOL;
}

//GETパラメータ"header"でoffを送ると以下のヘッダー行を送らないようにできる。
if ($mlscompatible == "on") {
    if ($header !== "off" && $gzip !== "on") {
        echo "radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal".PHP_EOL;
    } elseif ($header !== "off" && $gzip == "on") {
        $content .= "radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal".PHP_EOL;
    }
} else {
    if ($header !== "off" && $gzip !== "on") {
        echo "radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal,locationsource".PHP_EOL;
    } elseif ($header !== "off" && $gzip == "on") {
        $content .= "radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal,locationsource".PHP_EOL;
    }
}

//0件以上抽出の場合
if (count($db_data) > 0) {
    //データの出力
    for($i=0;$i<$count;$i++){

        if ($mlscompatible == "on") {
            //radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal
            if ($gzip !== "on") {
                echo $db_data[$i]['radio'].",".$db_data[$i]['mcc'].",".$db_data[$i]['net'].",".$db_data[$i]['area'].",".$db_data[$i]['cell'].",".$db_data[$i]['unit'].",".$db_data[$i]['lon'].",".$db_data[$i]['lat'].",".$db_data[$i]['ranges'].",".$db_data[$i]['samples'].",".$db_data[$i]['changeable'].",".$db_data[$i]['created'].",".$db_data[$i]['updated'].",".PHP_EOL;
            } else {
                $content .= $db_data[$i]['radio'].",".$db_data[$i]['mcc'].",".$db_data[$i]['net'].",".$db_data[$i]['area'].",".$db_data[$i]['cell'].",".$db_data[$i]['unit'].",".$db_data[$i]['lon'].",".$db_data[$i]['lat'].",".$db_data[$i]['ranges'].",".$db_data[$i]['samples'].",".$db_data[$i]['changeable'].",".$db_data[$i]['created'].",".$db_data[$i]['updated'].",".PHP_EOL;
            }
        } else {
            //radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal,locationsource
            if ($gzip !== "on") {
                echo $db_data[$i]['radio'].",".$db_data[$i]['mcc'].",".$db_data[$i]['net'].",".$db_data[$i]['area'].",".$db_data[$i]['cell'].",".$db_data[$i]['unit'].",".$db_data[$i]['lon'].",".$db_data[$i]['lat'].",".$db_data[$i]['ranges'].",".$db_data[$i]['samples'].",".$db_data[$i]['changeable'].",".$db_data[$i]['created'].",".$db_data[$i]['updated'].",".$db_data[$i]['averageSignal'].",".$db_data[$i]['locationsource'].PHP_EOL;
            } else {
                $content .= $db_data[$i]['radio'].",".$db_data[$i]['mcc'].",".$db_data[$i]['net'].",".$db_data[$i]['area'].",".$db_data[$i]['cell'].",".$db_data[$i]['unit'].",".$db_data[$i]['lon'].",".$db_data[$i]['lat'].",".$db_data[$i]['ranges'].",".$db_data[$i]['samples'].",".$db_data[$i]['changeable'].",".$db_data[$i]['created'].",".$db_data[$i]['updated'].",".$db_data[$i]['averageSignal'].",".$db_data[$i]['locationsource'].PHP_EOL;
            }
        }
        
    }
    //$gzipがonの場合はここですべての出力をgz圧縮してクライアントに渡す。
    //おそらくgzipがonでdebugもonの場合はContent-Length ヘッダーが破損する（Header行が何らかのechoの前にないといけないため）
    if ($gzip == "on") {
        $gzipcontent = gzencode($content);
        header('Content-Length: '.strlen($gzipcontent));
        echo $gzipcontent;
    }
}

dbdisconnect($db_session);
?>