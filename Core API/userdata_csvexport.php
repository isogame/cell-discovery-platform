<?php

//MySQL接続
require_once("../database_connector.php");
$db_session = dbconnect();

header('Content-Type: text/plain');

//curlとかから叩くならログイン中のCookie依存ではなくてログインIDをパラメータ渡しされてくる想定のほうがユースケースとしては多そう
$loginid=escs($db_session,$_GET["loginid"]);

//loginidがGETパラメータから読めなかった場合はCookieからの読み込みも試みる
if ($loginid == "" and $_COOKIE['loginid'] !== "") {
        $loginid = escs($db_session,$_COOKIE['loginid']);
}

if ($loginid == "" ) {
    echo "Not Allow Anonymous Login;";
    exit();
}

$sqlquery = "select * from queue_data where uploadid in ( select uploadid from queue_list where userid = '".$loginid."') group by cellid order by cancelReason ASC, timeStamp DESC, uploadedtime = '0000-00-00 00:00:00' DESC,uploadedtime desc,cellId ASC";
//$sqlquery = "select * from queue_data where uploadid in ( select uploadid from queue_list where userid = '".$loginid."' and uploadtimejst > now() - interval 6 day ) group by cellid order by cancelReason ASC, uploadedtime = '0000-00-00 00:00:00' DESC,uploadedtime desc,cellId ASC";
//echo $sqlquery.PHP_EOL;

echo "timestamp,radioType,mobileCountryCode,mobileNetworkCode,locationAreaCode,primaryScramblingCode,cellId,uploadCancel,cancelReason".PHP_EOL;

unset($db_result);
if ($result = $db_session->query($sqlquery)) {
	while ($row_data = $result->fetch_assoc()) {
		$db_result[] = $row_data;
	}
	$result->free();
}

$datacount = count($db_result);
if ($datacount > 0) {
    for($rec=0; $rec < count($db_result); $rec++) {
        echo "{$db_result[$rec]['timestamp']},{$db_result[$rec]['radioType']},{$db_result[$rec]['mobileCountryCode']},{$db_result[$rec]['mobileNetworkCode']},{$db_result[$rec]['locationAreaCode']},{$db_result[$rec]['primaryScramblingCode']},{$db_result[$rec]['cellId']},{$db_result[$rec]['uploadCancel']},{$db_result[$rec]['cancelReason']}".PHP_EOL;
    }
}
//MySQL接続解除
dbdisconnect($db_session);
?>