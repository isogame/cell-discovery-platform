<?php
    //MySQL接続
    require_once("../database_connector.php");
    $db_session = dbconnect();
    $postdata = file_get_contents("php://input");
    //$postdata = file_get_contents("testfile.txt");
    //echo $postdata;
    $mlsdata = json_decode($postdata, true);
    //$uploadid = md5(uniqid(rand(), true));
    $uploadid = md5($postdata);
    //Eecho "Upload ID:".$uploadid.PHP_EOL;
    $uploadtime = time();
    //Eecho "Upload Time:".$uploadtime.PHP_EOL;
    $uploadtimejst = (new DateTime('Asia/Tokyo'))->setTimestamp($uploadtime)->format("Y/m/d H:i:s");
    //Eecho "Upload Time JST:".$uploadtimejst.PHP_EOL;
    //Eecho "Array Count:".count($mlsdata["items"]).PHP_EOL;
    $itemcount = count($mlsdata["items"]);

    $userid = escs($db_session,$_GET["key"]);

    //POSTが空であればd41d8cd98f00b204e9800998ecf8427eがハッシュ値になるので、Direct Upload URLへの直アクセスが想定される。
    if ($uploadid == "d41d8cd98f00b204e9800998ecf8427e") {
        if ($userid !== "") {
            echo "<html><body>Your Cell Discovery Platform User ID is...<BR>".PHP_EOL;
            echo $userid."<BR>".PHP_EOL;
            echo "Your Cell Discovery Platform Direct Upload URL is...<BR>".PHP_EOL;
            echo "https://44011.brave-hero.net/api/v1/geosubmit?key=".$userid."<BR>".PHP_EOL;
            echo "<a href=\"#\" onclick=\"navigator.clipboard.writeText('https://44011.brave-hero.net/api/v1/geosubmit?key=".$userid."').then( alert('Copy OK') );\">Copy</a>";
            echo "</body></html>";
        } else {
            echo "Data Empty.".PHP_EOL;
        }
        return('');
    }

    //同じハッシュになるアップロードデータが既に登録されていたら、やめる
    //ただしここで200以外の返答をしてしまうと、Tower Collectorは未送信アップロードとして再度扱ってしまう。Tower Collectorからは消えないと困る。
    $sqlquery = "SELECT uploadid from queue_list where uploadid = '".$uploadid."'";
    $result = $db_session->query($sqlquery);
    if ($database_response = $result->fetch_array(MYSQLI_ASSOC)) {
        echo "This Upload Data as Exist. Goodbye or Die...".PHP_EOL;
        return('');
    }
    //Tower Collector Alternative からの送信の場合は現状、共通OpenCellId.orgのキー"pk.c1d907d5db4414943537b980adb0cf1f"が送られてくる場合がある
    if ($userid == "" OR $userid == "pk.c1d907d5db4414943537b980adb0cf1f") {
        //$userid = "Anonymous";
        http_response_code(400);
        echo "UserKeyNotFound;";
        return('');
    }
    //すでにこのユーザーIDのデータが登録されているか確認
    $sqlquery = "SELECT userid,point,cell,exclude44053,only44011,speedunlimit from user_list where userid = '".$userid."'";
    $result = $db_session->query($sqlquery);
    if ($database_response = $result->fetch_array(MYSQLI_ASSOC)) {
	    $pointcount = $database_response["point"];
        $cellcount = $database_response["cell"];

        $exclude44053 = $database_response["exclude44053"];
        $only44011 = $database_response["only44011"];
        $speedunlimit = $database_response["speedunlimit"];

        //Load-Exclude-eNB-List
        $sqlquery = "SELECT * from user_execludeenblist where userid = '".$userid."'";
        unset($excludeenbdata); 
        if ($result = $db_session->query($sqlquery)) {         
	        while ($row = $result->fetch_assoc()) {
		        $excludeenbdata[] = $row;
	        }
	        $result->free();
        }
        $excludeenbdata_count = count($excludeenbdata);

        //Load-Geofencing-Area
        $sqlquery = "SELECT * from user_geofencinglist where userid = '".$userid."'";
        unset($geofencinglist); 
        if ($result = $db_session->query($sqlquery)) {         
	        while ($row = $result->fetch_assoc()) {
		        $geofencinglist[] = $row;
	        }
	        $result->free();
        }
        $geofencinglist_count = count($geofencinglist);

        //Load-Upload-Options
        //ここにユーザー設定データが入っている予定だったが、現状は user_list のカラムでおさまっている
        // $sqlquery = "SELECT * from user_settingdata where userid = '".$userid."'";
        // unset($user_settingdata); 
        // if ($result = $db_session->query($sqlquery)) {         
	    //     while ($row = $result->fetch_assoc()) {
		//         $user_settingdata[] = $row;
	    //     }
	    //     $result->free();
        // }
        // $settingvalue_count = count($user_settingdata);
        // if ($settingvalue_count > 0) {
        //     for($i=0;$i<$geofencinglist_count;$i++){
        //         $$user_settingdata[$i]['datatype'] = $user_settingdata[$i]['value'];
        //         // switch ($user_settingdata[$i]['datatype']) {
        //         //     Case "noupload44053":
        //         //         $noupload44053 = $user_settingdata[$i]['value'];
        //         //         break;
        //         //     Case "uploadonly44011":
        //         //         $uploadonly44011 = $user_settingdata[$i]['value'];
        //         //         break;
        //         //     Case "speedunlimit":
        //         //         $speedunlimit = $user_settingdata[$i]['value'];
        //         //         break;
        //         // }
        //     }
        // }

    } else {
	    //新規ユーザーの場合は、新規登録日とユーザーIDをuser_listテーブルに挿入
        //Tower Collector Alternativeの場合はこちらに入ることがあるかも
	    //$sqlquery = "INSERT INTO user_list (userid,point,cell,register_date,lastupdate_date) VALUES ('".$userid."','0','0','".date("Y-m-d")."','".date("Y-m-d")."')";
	    //if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }
        //$pointcount = 0;
        //$cellcount = 0;

        //とか言っていたが、とりあえずキー不正は403にして返答
        http_response_code(403);
        echo "UserUnknown; Invalid ID Detected. Please Retry ID Create.";
        return('');
    }//新規ユーザー or 既存ユーザーおわり

    //Backup File Export
    //file_put_contents(__DIR__.'/MLSDiffRaw2/'.'queue_'.date('Ymd_His').'_'.$userid."_".$uploadid, var_dump(getallheaders()));
    file_put_contents(__DIR__.'/QueueRaw/'.'queue_'.date('Ymd_His').'_'.$userid."_".$uploadid, file_get_contents("php://input"));

    //Insert Upload Queue General Data
    //queue_listにuploadidを書く。queuestatusは0＝データ処理中にしておく
    $sqlquery = "INSERT INTO queue_list (userid,uploadid,uploadtimejst,queuestatus) VALUES ('".$userid."','".$uploadid."','".$uploadtimejst."','0')";
    if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }
    //ItemLoop
    $cellId="";
    for ($currentitem = 0; $currentitem <= $itemcount-1; $currentitem++) {
        $itemid = md5(uniqid(rand(), true));//このIDは最終的に使われないはずだが、cellTowersが0のときにどうなるかわからないのでやっている
        $pointcount++;
        $timestamp=escs($db_session,$mlsdata["items"][$currentitem]["timestamp"]);
        $latitude=escs($db_session,$mlsdata["items"][$currentitem]["position"]["latitude"]);
        $longitude=escs($db_session,$mlsdata["items"][$currentitem]["position"]["longitude"]);
        $accuracy=escs($db_session,$mlsdata["items"][$currentitem]["position"]["accuracy"]);
        $altitude=escs($db_session,$mlsdata["items"][$currentitem]["position"]["altitude"]);
        $heading=escs($db_session,$mlsdata["items"][$currentitem]["position"]["heading"]);
        $speed=escs($db_session,$mlsdata["items"][$currentitem]["position"]["speed"]);
        $source=escs($db_session,$mlsdata["items"][$currentitem]["position"]["source"]);

        //この後の処理で引っかかるとこれが1になってアップロードされないitemになる予定
        $uploadCancel = 0;
        //キャンセル理由コード(0=キャンセルされていない、1=手動でのキャンセル、2=除外eNB-LCIDにヒット、3=ジオフェンシングにヒット、4=44053をアップロードしない条件にヒット、5=44011以外はアップロードしない条件にヒット)
        //1はこの一連の処理では発生せず、Web画面からユーザーが手動でキャンセル操作をした場合に記録される
        $cancelReason = 0;

        //Speed Check
        //ここのspeedがメートル毎秒だったような気がしているが要調査。50m/s（175km/h）以下にする必要がある
        if ($speedunlimit == "1" and $speed > 50) {
            $speed = "";
             //Debug
             //$cancelReason = 9;
        }
        
        // Exclude Geofencing
        if ($geofencinglist_count > 0) {
            for($i=0;$i<$geofencinglist_count;$i++){
                if (GeoDistance($latitude,$longitude,$geofencinglist[$i]['lat'],$geofencinglist[$i]['lng'],"14") < $geofencinglist[$i]['radius']) {
                    $uploadCancel = 1;
                    $cancelReason = 3;
                }
            }
        }
        
        $celltowercount = count($mlsdata["items"][$currentitem]["cellTowers"]);
        //echo $celltowercount." cells.".PHP_EOL.PHP_EOL;
        $currentcell = 0;
        //0to0がほとんどだが、デュアルSIMの場合にcelltowercountが2で2-1。0to1になる。そうなる場合は1地点において2つのデータを書き込む必要がある。
        for ($currentcell = 0; $currentcell <= $celltowercount-1; $currentcell++) {
            $itemid = md5(uniqid(rand(), true));//これがitemidとして採用される
            //echo $currentcell.PHP_EOL;
            $radioType=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["radioType"]);
            $mobileCountryCode=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["mobileCountryCode"]);
            $mobileNetworkCode=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["mobileNetworkCode"]);
            $locationAreaCode=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["locationAreaCode"]);
            if (!($cellId == escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["cellId"]))) {
                $cellcount++;
            }
            $cellId=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["cellId"]);
            $primaryScramblingCode=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["primaryScramblingCode"]);
            $asu=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["asu"]);
            $signalStrength=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["signalStrength"]);
            $timingAdvance=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["timingAdvance"]);
            $serving=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][$currentcell]["serving"]);

            //uploadcancelフラグの初期化に気を付ける。
            //SIM1がau。楽天以外アップロードしない設定がされていてuploadcancelが1でcancelreasonが5になる。アップロード除外でデータを書き込み。
            //SIM2が楽天。このまま進むとuploadcancelが1なので楽天なのにアップロードされない。そこでループのたびにuploadcancelとcancelreasonを0に初期化。解決。
            //だがここまでにジオフェンスが原因でuploadcancelが1でcancelreasonが3の場合、ジオフェンスによるキャンセルが解除されてアップロードされてしまう。事故。
            //こういうことを避けるためにcancelReasonが3以外のときはフラグを初期化する。
            if ($cancelReason !== 3) {
                $uploadCancel = 0;
                $cancelReason = 0;
            }

            //Exclude eNB
            if ($excludeenbdata_count > 0) {
                for($i=0;$i<$excludeenbdata_count;$i++){
                    if ($excludeenbdata[$i]['MCC'] == $mobileCountryCode and $excludeenbdata[$i]['MNC'] == $mobileNetworkCode and $excludeenbdata[$i]['cellid'] == $cellId) {
                        $uploadCancel = 1;
                        $cancelReason = 2;
                    }
                }
            }

            //Discard 44053 Data
            if ($exclude44053 == "1" and $mobileCountryCode == "440" and $mobileNetworkCode == "53") {
                $uploadCancel = 1;
                $cancelReason = 4;
            }

            //Only 44011 Upload!
            if ($only44011 == "1" and ($mobileCountryCode !== "440" OR $mobileNetworkCode !== "11")) {
                $uploadCancel = 1;
                $cancelReason = 5;
            }

            //Insert Queue Data
            $sqlquery = "INSERT INTO queue_data (uploadid,itemid,timestamp,latitude,longitude,accuracy,altitude,heading,speed,source,radioType,mobileCountryCode,mobileNetworkCode,locationAreaCode,cellId,primaryScramblingCode,asu,signalStrength,timingAdvance,serving,uploadCancel,cancelReason) VALUES ('".$uploadid."','".$itemid."','".$timestamp."','".$latitude."','".$longitude."','".$accuracy."','".$altitude."','".$heading."','".$speed."','".$source."','".$radioType."','".$mobileCountryCode."','".$mobileNetworkCode."','".$locationAreaCode."','".$cellId."','".$primaryScramblingCode."','".$asu."','".$signalStrength."','".$timingAdvance."','".$serving."','".$uploadCancel."','".$cancelReason."')".PHP_EOL;
            if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }

            //CDP Full Collection
            if ($uploadCancel == "0") {
                $sqlquery = "INSERT INTO FullCollection (timestamp,latitude,longitude,accuracy,altitude,heading,speed,source,radioType,mobileCountryCode,mobileNetworkCode,locationAreaCode,cellId,primaryScramblingCode,asu,signalStrength,timingAdvance,serving,CreatedTime) VALUES ('".$timestamp."','".$latitude."','".$longitude."','".$accuracy."','".$altitude."','".$heading."','".$speed."','".$source."','".$radioType."','".$mobileCountryCode."','".$mobileNetworkCode."','".$locationAreaCode."','".$cellId."','".$primaryScramblingCode."','".$asu."','".$signalStrength."','".$timingAdvance."','".$serving."','".date('Y-m-d H:i:s')."')".PHP_EOL;
                if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }
            }
        }
    }

    //Queue Data Update
    //このデータは処理してもよい状態になったので、queuestatusを1に変更
    $sqlquery = "UPDATE queue_list SET queuestatus = '1' WHERE userid='".$userid."' and uploadid='".$uploadid."'";
    if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }

    //User Data Update
    $sqlquery = "UPDATE user_list SET point = '".$pointcount."', cell = '".$cellcount."', lastupdate_date = '".date("Y-m-d")."', upload_count = upload_count +1 WHERE userid='".$userid."'";
    if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }

    echo "Creating Pachimon MLS Data Analyze for Upload ID (".$uploadid.") Started.".PHP_EOL;


    //ここからパチモンMLS用のデータを作成する処理
    //セルごとにGROUP BYする
    //TAが全データで必ず0になってしまう機種検出用に、GROUP BYしていないSELECTを行ってTAの列をsumして0かどうか判別するような処理もこのあと必要かも
    $sqlquery = "SELECT * from queue_data where uploadid = '".$uploadid."' and uploadCancel = '0' GROUP BY mobileCountryCode, mobileNetworkCode, cellId";
    echo $sqlquery.PHP_EOL;
    unset($db_data); 
    if ($result = $db_session->query($sqlquery)) {         
        while ($row = $result->fetch_assoc()) {
        $db_data[] = $row;
        }
        $result->free();
    }
    
    $count = count($db_data);
    echo "Queue ".$uploadid." Items Count: ".$count.PHP_EOL.PHP_EOL;
    //uploadidごとに処理する。このuploadidで抽出して電測データが1件以上あれば次にすすむし、なければ（そんなことはないはず）elseに入ってNot Available, Skippedに入る
    if ($count > 0) {
        //cellIdループ
        for($i=0;$i<$count;$i++){
            echo "Processing Cell Data #".$i.": ".PHP_EOL;
            //今処理中のcellIdがMLSfinalに存在しているかどうかを確認する。
            //Deleted (MLSに登録されているセルが廃局になっていると1になるフラグ) が0であるデータに限定して検索。
            echo "Checking for MLSfinal Table...".PHP_EOL;
            $sqlquery = "SELECT * from MLSfinal where mcc='".$db_data[$i]['mobileCountryCode']."' and net='".$db_data[$i]['mobileNetworkCode']."' and cell='".$db_data[$i]['cellId']."' and deleted='0'";
            echo $sqlquery.PHP_EOL;
            unset($db_data_mlsfinal); 
            if ($result = $db_session->query($sqlquery)) {         
                while ($row = $result->fetch_assoc()) {
                $db_data_mlsfinal[] = $row;
                }
                $result->free();
            }
            //パチモンMLSの元ネタデータベースへ問い合わせ、RSRPやRSRP+TAで位置を算出して averageLocation へ帰ってくる
            //2024.07.01 以前のTODO: ここまでにFullCollectionにINSERTしてFullCollectionから読んだほうがいいな
            //2024.07.01 に変更: SELECT条件に現在処理中のLACを追加し最新のLACのデータだけをデータソースとして生成するようにする。
            echo "Calculate Cell Location...".PHP_EOL;
            $sqlquery = "SELECT latitude, longitude, asu, (-140+asu*2) as RSRP, timingAdvance from FullCollection where mobileCountryCode = '".$db_data[$i]['mobileCountryCode']."' and mobileNetworkCode = '".$db_data[$i]['mobileNetworkCode']."' and locationAreaCode = '".$db_data[$i]['locationAreaCode']."' and cellId = '".$db_data[$i]['cellId']."' and deleted = '0'";
            //$sqlquery = "SELECT latitude, longitude, asu, (-140+asu*2) as RSRP, timingAdvance from FullCollection where mobileCountryCode = '".$db_data[$i]['mobileCountryCode']."' and mobileNetworkCode = '".$db_data[$i]['mobileNetworkCode']."' and cellId = '".$db_data[$i]['cellId']."'";
            echo $sqlquery.PHP_EOL;
            unset($db_data_fullcollection); 
            if ($result = $db_session->query($sqlquery)) {         
                while ($row = $result->fetch_assoc()) {
                $db_data_fullcollection[] = $row;
                }
                $result->free();
            }
            echo "Available Full Collection Data Sample Count: ".count($db_data_fullcollection).PHP_EOL;
            //CalculateCellLocationにどんなデータが渡っているか確認用
            //var_dump($db_data_fullcollection);
            $averageLocation = CalculateCellLocation($db_data_fullcollection,156);//ここの156は1TAあたりのメートル。本当はユーザーデータからちゃんととってくるべき
    
            //AverageSignal列に入れるデータを計算
            if (count($db_data_fullcollection) > 0) {
                $SignalValues = array_column($db_data_fullcollection, 'RSRP');
                $SignalSum = array_sum($SignalValues);
                $SignalCount = count($SignalValues);
                $paverageSignal = floor($SignalSum / $SignalCount);
                echo "Average Signal: ".$paverageSignal.PHP_EOL;
            }
            //MLSにすでに存在しているセルなら、MLSの場所を返す。MLSに存在していてもパチモンMLSに999以上のサンプルがあるか、MLSに存在しない新規セルなら averageLocation に入ってる緯度経度を使う
            if (count($db_data_mlsfinal) > 0 and count($db_data_fullcollection) < 999) {
                $isExistsCell = True;//これは使われていない
                echo "Cell is Exists MLSfinal, Uses MLS Database Location".PHP_EOL;
                //このセルの位置情報は、MLSfinalからとってきた座標が使われる。
                $plat = $db_data_mlsfinal[0]['lat'];
                $plon = $db_data_mlsfinal[0]['lon'];
                $pcreated = $db_data_mlsfinal[0]['created'];
                $plocationsource = "mls";
            } else {
                $isExistsCell = False;//これは使われていない
                //どうしてelseに入ったか詳細な条件を確認
                if (count($db_data_mlsfinal) > 0) {
                    echo "Cell is Exists MLSfinal... but Available > 999 Samples PachimonMLS. Use PachimonMLS Database Location".PHP_EOL;
                } else {
                    echo "Cell is Not Exists MLSfinal. Use PachimonMLS Database Location".PHP_EOL;
                }
                //CalculateCellLocation を呼んだ結果算出された緯度経度
                $plat = $averageLocation['lat'];
                $plon = $averageLocation['lng'];
                $pcreated = time(); //現在のUNIX TimeをCreatedの時刻とする。
                $plocationsource = "cdp";
            }
            //どちらにせよ独自算出の緯度経度は記録するので書き出し
            //サンプル数が多い場合　および　新規セルの場合は lat,lon と rlat,rlon は同じになるはずなので、いずれ rlat,rlon は削除しなければならない（データ量が莫大になるので）
            $rlat = $averageLocation['lat'];
            $rlon = $averageLocation['lng'];
            $rtlat = $averageLocation['rtlat'];
            $rtlon = $averageLocation['rtlng'];
            //そのほか共通のデータ
            $pradio = strtoupper($db_data[$i]['radioType']);
            $pmcc = $db_data[$i]['mobileCountryCode'];
            $pmnc = $db_data[$i]['mobileNetworkCode'];
            $plac = $db_data[$i]['locationAreaCode'];
            $punit = $db_data[$i]['primaryScramblingCode'];
            $pupdated = time(); //現在のUNIX TimeをUpdatedの時刻とする。
            
            //PachimonMLSテーブルへの書き出し
            $sqlquery = "SELECT * from DistributeCollection where mcc='".$db_data[$i]['mobileCountryCode']."' and net='".$db_data[$i]['mobileNetworkCode']."' and cell='".$db_data[$i]['cellId']."'";
            echo $sqlquery.PHP_EOL;
            unset($db_data_availablepachimonmls); 
            if ($result = $db_session->query($sqlquery)) {         
                while ($row = $result->fetch_assoc()) {
                $db_data_availablepachimonmls[] = $row;
                }
                $result->free();
            }
            if (count($db_data_availablepachimonmls) > 0) {
                //Update PachimonMLS Data
                $sqlquery = "UPDATE DistributeCollection SET updated = '".$pupdated."',unit = '".$punit."',lat = '".$plat."',lon = '".$plon."',rlat = '".$rlat."',rlon = '".$rlon."',rtlat = '".$rtlat."',rtlon = '".$rtlon."',samples = '".count($db_data_fullcollection)."',averageSignal = '".$paverageSignal."',locationsource = '".$plocationsource."' WHERE mcc='".$db_data[$i]['mobileCountryCode']."' and net='".$db_data[$i]['mobileNetworkCode']."' and cell='".$db_data[$i]['cellId']."'";
                echo $sqlquery . PHP_EOL;
                if (!$result = $db_session->query($sqlquery)) { echo "SQL Execution Error Occurred: ".__line__.PHP_EOL; }
            } else {
                //Insert PachimonMLS Data
                $sqlquery = "INSERT INTO DistributeCollection (radio,mcc,net,area,cell,unit,lon,lat,rlon,rlat,rtlon,rtlat,ranges,samples,changeable,created,updated,averageSignal,locationsource) VALUES ('".$pradio."','".$pmcc."','".$pmnc."','".$plac."','".$db_data[$i]['cellId']."','".$punit."','".$plon."','".$plat."','".$rlon."','".$rlat."','".$rtlon."','".$rtlat."','0','".count($db_data_fullcollection)."','1','".$pcreated."','".$pupdated."','".$paverageSignal."','".$plocationsource."')";
                echo $sqlquery . PHP_EOL;
                if (!$result = $db_session->query($sqlquery)) { echo "SQL Execution Error Occurred: ".__line__.PHP_EOL; }
            }
            echo "Cell Process Complete, Come on Next Cell !!".PHP_EOL.PHP_EOL;
        }
        echo "All Cell Process Complete.".PHP_EOL.PHP_EOL;
        
        dbdisconnect($db_session);
    } else {
        echo "Item Not Available, Skipped.".PHP_EOL;
    }
    
    //var_dump($mlsdata);
    //MySQL接続解除
    dbdisconnect($db_session);

    echo "All Process Done!".PHP_EOL;
    //MLSのgeosubmit v2 API互換にするなら、この応答だけechoする必要がある。すでにUploadidとかいろんな情報をechoしてしまっているので、現在は厳密にはMLS互換ではない
    //Tower CollectorはHTTP 200 OKかどうかを正常に受け付けられたかどうかの判定に使っているので、それ以外の応答があっても問題ない。MLS Upload Helperはこのエンドポイントにアクセスしないし、MLS Upload HelperのAPIエンドポイントからこのエンドポイントに送るときもそこは見ていない。
    echo "{}";

    //測地線航海算法を利用した2点間の緯度経度から距離を取得するファンクション
    //outline: http://webdesign-dackel.com/2015/01/18/google-maps-api/
    //for javascript: http://hamasyou.com/blog/2010/09/07/post-2/
    //for php: http://www.cpslabo.com/blog/index.php?e=283

    //lat1,lng1= Measurement Geolocation
    //lat2,lng2= Cell Tower Geolocation
    function GeoDistance($lat1,$lng1,$lat2,$lng2,$decimal){
        // 引数　$decimal は小数点以下の桁数
        if ( (abs($lat1-$lat2) < 0.00001) && (abs($lng1-$lng2) < 0.00001) ) {
            $distance = 0;
        } else {
            $lat1 = $lat1*pi()/180;$lng1 = $lng1*pi()/180;
            $lat2 = $lat2*pi()/180;$lng2 = $lng2*pi()/180;
    
            $A = 6378140;
            $B = 6356755;
            $F = ($A-$B)/$A;
    
            $P1 = atan(($B/$A)*tan($lat1));
            $P2 = atan(($B/$A)*tan($lat2));
    
    
            $X = acos( sin($P1)*sin($P2) + cos($P1)*cos($P2)*cos($lng1-$lng2) );
            $L = ($F/8)*( (sin($X)-$X)*pow((sin($P1) + sin($P2)),2)/pow(cos($X/2) ,2) - (sin($X)-$X)*pow(sin($P1)-sin($P2),2)/pow(sin($X),2) );
    
            $distance = $A*($X+$L);
            $decimal_no=pow(10,$decimal);
            $distance = round($decimal_no*$distance)/$decimal_no;//ここでdistanceに/1000を入れるとkmになる
        }
        $format='%0.'.$decimal.'f';
        return sprintf($format,$distance);
    }

    function CalculateCellLocation($datasets,$DeclareTimingAdvance) {
        $weightedLatSum = 0;
        $weightedLngSum = 0;
        $totalWeight = 0;
        $TAweightedLatSum = 0;
        $TAweightedLngSum = 0;
        $TAtotalWeight = 0;
        foreach ($datasets as $index => $data) { //最初は$datasets as $dataと書いてあったがindex番号を取得したいがために左のように変わった
            $rsrp = abs($data['RSRP']); //-140+ASUがRSRP。RSRPをSQLで算出できない場合はここで計算しておく　とおもったが、そもそもasu*2しないと正確なRSRPが出ない疑惑あり・・・
            $rsrpWeight = pow((1.0 / (pow($rsrp + 5.0, 2))) * 10000, 2);
            $taWeight = !empty($data['timingAdvance']) ? 1.0 / (1 + ($data['timingAdvance'] * $DeclareTimingAdvance * 0.01)) : 0.03;
            
            // 結果の表示
            echo "Data #" . ($index + 1) . ": ";
            echo "RSRP: " . number_format($rsrp, 0) . ", ";
            echo "TA: " . number_format($data['timingAdvance'], 0) . ", ";
            echo "rsrp_weight: " . number_format($rsrpWeight, 6) . ", ";
            echo "ta_weight: " . number_format($taWeight, 6) . ", ";
            echo "total_weight: " . number_format($rsrpWeight * $taWeight, 6) . PHP_EOL;
            
            // 実際に使われる数値
            $weightedLatSum += $data['latitude'] * $rsrpWeight;
            $weightedLngSum += $data['longitude'] * $rsrpWeight;
            $totalWeight += $rsrpWeight;
            //実験: TA値をウェイトに加算した場合の位置も出力
            $TAweightedLatSum += $data['latitude'] * $rsrpWeight * $taWeight;
            $TAweightedLngSum += $data['longitude'] * $rsrpWeight * $taWeight;
            $TAtotalWeight += $rsrpWeight * $taWeight;
        }
        //実際に使われる緯度経度
        $averageLat = $weightedLatSum / $totalWeight;
        $averageLng = $weightedLngSum / $totalWeight;
        
        //実験の緯度経度
        $TAaverageLat = $TAweightedLatSum / $TAtotalWeight;
        $TAaverageLng = $TAweightedLngSum / $TAtotalWeight;
        
        echo "Cell Location: ".$averageLat . ", " . $averageLng . PHP_EOL;
        //実験の結果
        echo "Cell Location with TA Weight: ".$TAaverageLat . ", " . $TAaverageLng . PHP_EOL;
        return ['lat' => $averageLat, 'lng' => $averageLng, 'rtlat' => $TAaverageLat, 'rtlng' => $TAaverageLng];
    }
    
?>