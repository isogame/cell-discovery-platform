<?php
    //APIバージョンの確認
    if ( $_SERVER['HTTP_X_API_VERSION'] !== "1" ) {
        echo "Unsupported; Unsupported API Version Request Detected. Please Upgrade MLS Upload Helper Client";
        exit;
    }

    //MySQL接続
    require_once("../database_connector.php");
    $db_session = dbconnect();
    $postdata = file_get_contents("php://input");
    //$postdata = file_get_contents("mls.json");
    //
    //echo $postdata;
    $mlsdata = json_decode($postdata, true);
    //$uploadid = md5(uniqid(rand(), true));
    $uploadid = md5($postdata);
    echo "Upload ID:".$uploadid;
    echo "<BR>";
    $uploadtime = time();
    echo "Upload Time:".$uploadtime;
    echo "<BR>";
    $uploadtimejst = (new DateTime('Asia/Tokyo'))->setTimestamp($uploadtime)->format("Y/m/d H:i:s");
    echo "Upload Time JST:".$uploadtimejst;
    echo "<BR>";
    echo "Array Count:".count($mlsdata["items"]);
    $itemcount = count($mlsdata["items"]);
    echo "<BR>";

    $sqlquery = "SELECT userid,nickname from user_list where userid = '".escs($db_session,$_GET["userid"])."'";
    $result = $db_session->query($sqlquery);
    if (!$database_response = $result->fetch_array(MYSQLI_ASSOC)) {
        echo "UserUnknown; Invalid ID Detected. Please Retry ID Create.";
        exit;
    }
    
    $userid = escs($db_session,$_GET["userid"]);
    if ($userid == "" OR $userid == "pk.c1d907d5db4414943537b980adb0cf1f") {
        $userid = "Anonymous";
    }
    //Backup File Export
    //1
    file_put_contents(__DIR__.'/MUFRaw/'.'queue_'.date('Ymd_His').'_'.$userid."_".$uploadid, file_get_contents("php://input"));
    
    //`items`のデータを80件ごとに分割
    $chunks = array_chunk($mlsdata['items'], 80);

    //各分割された配列を再びJSONにエンコード
    for ($i = 0; $i < count($chunks); $i++) {
        $outputData = ['items' => $chunks[$i]];
        //debug
        //file_put_contents($filename, json_encode($outputData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        //file_put_contents($filename, json_encode($outputData));
        $senddata = json_encode($outputData);
        //2
        file_put_contents(__DIR__.'/MUFRaw_sp/'.'queue_'.date('Ymd_His').'_'.$userid."_".$uploadid, $senddata);
        $context = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => implode("\r\n", array('Content-Type: application/json; charset=utf-8',)),
                'content' => $senddata
            )
        );
        //3
        $httpresult = file_get_contents("https://44011.brave-hero.net/api/geosubmit.php?key=".$userid, false, stream_context_create($context));
        file_put_contents(__DIR__.'/MUFRaw_result/'.'result_'.date('Ymd_His').'_'.$userid."_".$uploadid, $httpresult);
    }
    
    //MySQL接続解除
    dbdisconnect($db_session);
?>