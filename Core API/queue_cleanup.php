<?php
date_default_timezone_set('Asia/Tokyo');
echo "Environment Check...".PHP_EOL;
echo "Working Directory: ".getcwd().PHP_EOL;
echo "File Directory: ".(__DIR__).PHP_EOL;
chdir(__DIR__);
require_once("../database_connector.php");

//MySQL接続
$db_session = dbconnect();
echo "Database Connected.".PHP_EOL;

echo "Starting Cleanup Progress...".PHP_EOL;
$db_session->begin_transaction();
try {
    // queue_dataから1カ月以上前のデータを削除
    $stmt1 = $db_session->prepare("DELETE FROM queue_data WHERE uploadid IN ( SELECT uploadid FROM (SELECT uploadid FROM queue_list WHERE uploadtimejst < (NOW() - INTERVAL 1 MONTH)) as limitresult )");
    $stmt1->execute();
    $stmt1->close();
    echo "Clean queue_data.".PHP_EOL;

    // queue_listから1カ月以上前のデータを削除
    $stmt2 = $db_session->prepare("DELETE FROM queue_list WHERE uploadtimejst < (NOW() - INTERVAL 1 MONTH)");
    $stmt2->execute();
    $stmt2->close();
    echo "Clean queue_list.".PHP_EOL;

    // トランザクションをコミット
    $db_session->commit();
    echo "Cleanup COMMIT.".PHP_EOL;

} catch (mysqli_sql_exception $exception) {
    // エラーがあった場合はロールバック
    $db_session->rollback();
    echo "Cleanup Rollback....".PHP_EOL;
    throw $exception;
}
echo "Finished Cleanup.".PHP_EOL;

//MySQL接続解除
dbdisconnect($db_session);
?>