<?php
//大量のデータを出力しようとするとHTTP 500エラーを返す場合があるので利用できるメモリを増やしてみる
//本番環境はmemory_limitが200M
ini_set('memory_limit', '2048M');
//MySQL接続
require_once("../database_connector.php");
$db_session = dbconnect();

$mcc=esch(escs($db_session,$_GET["mcc"]));
$mnc=esch(escs($db_session,$_GET["mnc"]));

$before=esch(escs($db_session,$_GET["before"]));
$after=esch(escs($db_session,$_GET["after"]));

$debug=esch(escs($db_session,$_GET["debug"]));
$gzip=esch(escs($db_session,$_GET["gzip"]));

$content = "";
$gzipcontent = "";

if ($gzip == "on") {
    ini_set('zlib.output_compression','Off');
    header('Content-Type: application/gzip');
    header('Content-Disposition: attachment; filename="cdp-distribute-collection-export.json.gz"');
    header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
    header('Pragma: no-cache');
} else {
    header('Content-Type: application/json');
}

if ($debug == "on") {
    echo "Welcome to Cell Discovery Platform.".PHP_EOL;
    echo "Debug mode is on. This output is incorrect as JSON data. This is a debugging mode for developers.".PHP_EOL;
    echo "Reuqest Parameter List:".PHP_EOL;
    echo "debug=".$debug.PHP_EOL;
    echo "mcc=".$mcc.PHP_EOL;
    echo "mnc=".$mnc.PHP_EOL;
    echo "before=".$before.PHP_EOL;
    echo "after=".$after.PHP_EOL;
    echo "gzip=".$gzip.PHP_EOL;
    echo PHP_EOL;
    echo "Run SQL Query: ";
}

//CDPから取得（あとの行でMLSもUNIONする）
$sqlquery = "SELECT lat,lon,area,cell,unit,samples,created,updated from DistributeCollection where 1=1";
//MLS最終データから引く場合はこれにする
//$sqlquery = "SELECT lat,lon,area,cell,unit,samples,created,updated from MLSfinal where 1=1";

//MCCが指定されている場合
if ($mcc !== "") {
    $sqlquery .= " and mcc = '".$mcc."'";
}

//MNCが指定されている場合
if ($mnc !== "") {
    $sqlquery .= " and net = '".$mnc."'";
}

//LTEのみにする
$sqlquery .= " and radio = 'LTE'";

//afterが指定されている場合
if ($after !== "") {
    $sqlquery .= " and updated >= '".$after."'";
}

//beforeが指定されている場合
if ($before !== "") {
    $sqlquery .= " and updated <= '".$before."'";
}

//+MLS UNION
$sqlquery = $sqlquery . " UNION " . str_replace("DistributeCollection","MLSfinal",$sqlquery);

if ($debug == "on") {
    echo $sqlquery.PHP_EOL;
}

unset($db_data); 
if ($result = $db_session->query($sqlquery)) {         
    while ($row = $result->fetch_assoc()) {
    $db_data[] = $row;
    }
    $result->free();
}

$count = count($db_data);
if ($debug == "on") {
    echo "Query Data Count: ".$count.PHP_EOL.PHP_EOL;
}

//JSON整形する場合は、JSON_NUMERIC_CHECK|JSON_PRETTY_PRINT
$content = json_encode( $db_data, JSON_NUMERIC_CHECK ) ;

if(json_last_error() == JSON_ERROR_NONE){
    //$gzipがonの場合はここですべての出力をgz圧縮してクライアントに渡す。
    //おそらくgzipがonでdebugもonの場合はContent-Length ヘッダーが破損する（Header行が何らかのechoの前にないといけないため）
    if ($gzip == "on") {
        //gzipファイル出力
        $gzipcontent = gzencode($content);
        header('Content-Length: '.strlen($gzipcontent));
        echo $gzipcontent;
    } else {
        //通常出力
        echo $content;
    }
} else {
    //http_response_code(500);
}

//MySQL接続解除
dbdisconnect($db_session);
?>