<?php
header('Content-Type: text/plain');

//MySQL接続
require_once("../database_connector.php");
$db_session = dbconnect();

//MLS管理表より廃止局リストを取得
//https://mls.js2hgw.com/api/change_date.php
$raw_data = file_get_contents('https://mls.js2hgw.com/api/change_date.php');
$dataarray = json_decode($raw_data,true);

//この段階でMLSfinalとFullCollectionのdeletedを一度すべて0にする処理をいれておきたい
//まだ書いていない

//Loop
foreach ($dataarray as $cellid => $date ) {
    echo "Processing ".$date . " Discomission Cell " . floor($cellid/256) . "-" . $cellid%256 . "..." . PHP_EOL;
    $date = new DateTime($date.' 00:00:00', new DateTimeZone('Asia/Tokyo'));
    //U=dateをUnixTimeにする。このままだとミリ秒なしのUnixTimeなのでミリ秒のvをつける。Example 2024-05-24 => 1716476400000
    $unixdt = $date->format('Uv') . '000';
    //MLS管理表から示された日付以前のデータにdeleted 1 を立てて、deleted 0 のデータをもとに DistributeCollection を再構築。
    $sqlquery = "UPDATE FullCollection SET deleted = '1' WHERE mobileCountryCode = '440' and mobileNetworkCode = '11' and cellId = '".$cellid."' and timestamp < '".$unixdt."'";
    echo $sqlquery . PHP_EOL;
    //有効化する
    if (!$result = $db_session->query($sqlquery)) { echo "SQL Execution Error Occurred: ".__line__.PHP_EOL; }
    
    echo "Calculate Cell Location...".PHP_EOL;
    $sqlquery = "SELECT latitude, longitude, asu, (-140+asu*2) as RSRP, timingAdvance from FullCollection where mobileCountryCode = '440' and mobileNetworkCode = '11' and cellId = '".$cellid."' and deleted = '0'";
    echo $sqlquery.PHP_EOL;
    unset($db_data_fullcollection); 
    if ($result = $db_session->query($sqlquery)) {         
        while ($row = $result->fetch_assoc()) {
            $db_data_fullcollection[] = $row;
            }
        $result->free();
    }
    echo "Available Full Collection Data Sample Count: ".count($db_data_fullcollection).PHP_EOL;

    echo "Check MLSfinal Available?...".PHP_EOL;
    $sqlquery = "SELECT count(*) from MLSfinal where mcc = '440' and net = '11' and cell = '".$cellid."' and deleted = '0'";
    echo $sqlquery.PHP_EOL;
    unset($db_data_mlsfinal); 
    if ($result = $db_session->query($sqlquery)) {         
        while ($row = $result->fetch_assoc()) {
            $db_data_mlsfinal[] = $row;
            }
        $result->free();
    }
    echo "Available MLSfinal Count: ".count($db_data_mlsfinal).PHP_EOL;

    //使っていいデータ(deleted=0)のみを使って緯度経度を再計算する
    //ここから
    $averageLocation = CalculateCellLocation($db_data_fullcollection,156);//ここの156は1TAあたりのメートル。本当はユーザーデータからちゃんととってくるべき
    
    //AverageSignal列に入れるデータを計算
    if (count($db_data_fullcollection) > 0) {
        $SignalValues = array_column($db_data_fullcollection, 'RSRP');
        $SignalSum = array_sum($SignalValues);
        $SignalCount = count($SignalValues);
        $paverageSignal = floor($SignalSum / $SignalCount);
        echo "Average Signal: ".$paverageSignal.PHP_EOL;
    }
    $plocationsource = "cdp";
    $rlat = $averageLocation['lat'];
    $rlon = $averageLocation['lng'];
    $rtlat = $averageLocation['rtlat'];
    $rtlon = $averageLocation['rtlng'];
    //ここまではgeosubmitと同様。geosubmitの場合はMLSfinalから引いてきたplon,platが出てくるが、こちらはgeosubmitの新規セルの場合と同様、rlon,rlatをlon,latに格納する。rlon,rlatおよびrtlon,rtlatも生きているデータをもとに再計算して格納しなおす

    //ここからもだいたい同様、データがないときにDistributeCollectionから消してしまう部分だけ違う
    //DistributeCollectionテーブルへの書き出し
    if (count($db_data_fullcollection) > 0) {
        //Update CDP DistributeCollection Data
        $sqlquery = "UPDATE DistributeCollection SET changedext = '1',lat = '".$rlat."',lon = '".$rlon."',rlat = '".$rlat."',rlon = '".$rlon."',rtlat = '".$rtlat."',rtlon = '".$rtlon."',samples = '".count($db_data_fullcollection)."',averageSignal = '".$paverageSignal."',locationsource = '".$plocationsource."' WHERE mcc='440' and net='11' and cell='".$cellid."'";
        echo $sqlquery . PHP_EOL;
        if (!$result = $db_session->query($sqlquery)) { echo "SQL Execution Error Occurred: ".__line__.PHP_EOL; }
    } else {
        //DELETE CDP DistributeCollection Data
        $sqlquery = "DELETE FROM DistributeCollection WHERE mcc='440' and net='11' and cell='".$cellid."'";
        echo $sqlquery . PHP_EOL;
        //if (!$result = $db_session->query($sqlquery)) { echo "SQL Execution Error Occurred: ".__line__.PHP_EOL; }
    }

    //MLSfinalテーブルに登録されているデータの無効化
    if (count($db_data_mlsfinal) > 0) {
        //Update CDP DistributeCollection Data
        $sqlquery = "UPDATE MLSfinal SET deleted = '1' WHERE mcc='440' and net='11' and cell='".$cellid."'";
        echo $sqlquery . PHP_EOL;
        if (!$result = $db_session->query($sqlquery)) { echo "SQL Execution Error Occurred: ".__line__.PHP_EOL; }
    }
    echo "Cell Process Complete, Come on Next Cell !!".PHP_EOL.PHP_EOL;
}

//ここのアルゴリズムもgeosubmitと同様　ライブラリ化してもいいかもね
function GeoDistance($lat1,$lng1,$lat2,$lng2,$decimal){
    // 引数　$decimal は小数点以下の桁数
    if ( (abs($lat1-$lat2) < 0.00001) && (abs($lng1-$lng2) < 0.00001) ) {
        $distance = 0;
    } else {
        $lat1 = $lat1*pi()/180;$lng1 = $lng1*pi()/180;
        $lat2 = $lat2*pi()/180;$lng2 = $lng2*pi()/180;

        $A = 6378140;
        $B = 6356755;
        $F = ($A-$B)/$A;

        $P1 = atan(($B/$A)*tan($lat1));
        $P2 = atan(($B/$A)*tan($lat2));


        $X = acos( sin($P1)*sin($P2) + cos($P1)*cos($P2)*cos($lng1-$lng2) );
        $L = ($F/8)*( (sin($X)-$X)*pow((sin($P1) + sin($P2)),2)/pow(cos($X/2) ,2) - (sin($X)-$X)*pow(sin($P1)-sin($P2),2)/pow(sin($X),2) );

        $distance = $A*($X+$L);
        $decimal_no=pow(10,$decimal);
        $distance = round($decimal_no*$distance)/$decimal_no;//ここでdistanceに/1000を入れるとkmになる
    }
    $format='%0.'.$decimal.'f';
    return sprintf($format,$distance);
}

function CalculateCellLocation($datasets,$DeclareTimingAdvance) {
    $weightedLatSum = 0;
    $weightedLngSum = 0;
    $totalWeight = 0;
    $TAweightedLatSum = 0;
    $TAweightedLngSum = 0;
    $TAtotalWeight = 0;
    foreach ($datasets as $index => $data) { //最初は$datasets as $dataと書いてあったがindex番号を取得したいがために左のように変わった
        $rsrp = abs($data['RSRP']); //-140+ASUがRSRP。RSRPをSQLで算出できない場合はここで計算しておく　とおもったが、そもそもasu*2しないと正確なRSRPが出ない疑惑あり・・・
        $rsrpWeight = pow((1.0 / (pow($rsrp + 5.0, 2))) * 10000, 2);
        $taWeight = !empty($data['timingAdvance']) ? 1.0 / (1 + ($data['timingAdvance'] * $DeclareTimingAdvance * 0.01)) : 0.03;
        
        // 結果の表示
        echo "Data #" . ($index + 1) . ": ";
        echo "RSRP: " . number_format($rsrp, 0) . ", ";
        echo "TA: " . number_format($data['timingAdvance'], 0) . ", ";
        echo "rsrp_weight: " . number_format($rsrpWeight, 6) . ", ";
        echo "ta_weight: " . number_format($taWeight, 6) . ", ";
        echo "total_weight: " . number_format($rsrpWeight * $taWeight, 6) . PHP_EOL;
        
        // 実際に使われる数値
        $weightedLatSum += $data['latitude'] * $rsrpWeight;
        $weightedLngSum += $data['longitude'] * $rsrpWeight;
        $totalWeight += $rsrpWeight;
        //実験: TA値をウェイトに加算した場合の位置も出力
        $TAweightedLatSum += $data['latitude'] * $rsrpWeight * $taWeight;
        $TAweightedLngSum += $data['longitude'] * $rsrpWeight * $taWeight;
        $TAtotalWeight += $rsrpWeight * $taWeight;
    }
    //実際に使われる緯度経度
    $averageLat = $weightedLatSum / $totalWeight;
    $averageLng = $weightedLngSum / $totalWeight;
    
    //実験の緯度経度
    $TAaverageLat = $TAweightedLatSum / $TAtotalWeight;
    $TAaverageLng = $TAweightedLngSum / $TAtotalWeight;
    
    echo "Cell Location: ".$averageLat . ", " . $averageLng . PHP_EOL;
    //実験の結果
    echo "Cell Location with TA Weight: ".$TAaverageLat . ", " . $TAaverageLng . PHP_EOL;
    return ['lat' => $averageLat, 'lng' => $averageLng, 'rtlat' => $TAaverageLat, 'rtlng' => $TAaverageLng];
}

//MySQL接続解除
dbdisconnect($db_session);

echo "All Process Done!".PHP_EOL;
?>