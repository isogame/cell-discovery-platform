<?php
require_once("databases/isogame_mlsdb.php");

function dbconnect() {
	$db_session = @new mysqli(connect_dbsvr,connect_dbusr,connect_dbpwd,connect_dbnam);
	if ($db_session->connect_error) {
		echo "error";
		exit();
	}
	$db_session->set_charset('utf8');
	//define("local_genuineid",escs($db_session,$_COOKIE['genuineid']));
	//define("rivalid_me",escs($db_session,$_COOKIE['rivalid_me']));
	return $db_session;
}

function dbdisconnect($db_session) {
	$db_session->close();
}

function esch($str) {
	return htmlspecialchars($str);
}

function escs($db_session,$str) {
	return $db_session->real_escape_string($str);
}

?>