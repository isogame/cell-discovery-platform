# Cell Discovery Platform
Cell Discovery Platform は Mozilla Location Service (MLS) がサービス終了することに伴い、電測職人のみなさまが引き続き Tower Collector を使ったセル位置情報の収集と共有を可能にするため、MLSと同等の機能を有するセル位置情報公開サービスとして機能することを目的に立ち上げられたプロジェクトです。

今までのMLSでできなかったこと、可視化されていなかった情報、電測職人のみなさまが欲しいものすべてを実現していきます。

# Cell Discovery Platform Core
Cell Discovery Platform Core は、電測職人のみなさまが Tower Collector を使って収集したデータの受信と、データの解析、解析済みデータの提供を行うコアコンポーネントです。

- geosubmit: MLSデータアップロードマネージャーのフォークです。Tower Collectorからの送信データを処理します。
- csvexport: MLS互換のデータを受信して活用するアプリケーションに向けてデータを提供するAPIです。
- muhsubmit: MLS Upload Helper からのデータを受信して geosubmit にデータを流すAPIです。
- queue_cleanup: 従来までMLSへのデータ反映を確認してからデータ消去していたものを、20日経過後に削除するように改良したクリーンアップスクリプトです。
- Core Web Application ディレクトリ: これらはMLSデータアップロードマネージャーのフォークです。geosubmitへ送信されたデータの閲覧とジオフェンスの設定などパーソナライズ設定を提供します。

# CDP Upload Helper
CDP Upload Helper は、MLS Data Upload Manager プロジェクトで提供されていた「MLS Upload Helper」のフォークです。Tower Collector にもともと存在するエクスポート機能を使って書き出された「Mozilla Location ServicesのAPI仕様に適合したJSON形式のデータ」を読み取ることができ、これを Cell Discovery Platform Core に送信することができます。

Tower Collector にて custom MLS Service 機能がサポートされて以降は不要ですが、予想外のことが起こった場合に備えてメンテナンスされています。