
// https://mls.js2hgw.com/wiki/?TAC より。有効TAC。 何らかのバグでMLSにゴミデータがあるのを除外するため。
const js2hgwTac = new Set([
    // 特殊TAC
    111,112,113,114,211,212,213,214,311,312,313,314,25511,25512,25513,25514,33411,
    // 都道府県別TAC
    1025,1026,1027,1028,1029,2049,2050,3073,3074,4097,4098,5121,5122,6145,6146,7169,7170,7171,8193,8194,8195,8196,9217,9218,10241,10242,11265,11266,11267,11268,12289,12290,12291,12292,13313,13314,13315,13316,15361,15362,15363,15364,15365,15366,16385,16386,16387,17409,18433,19457,20481,21505,21506,21507,22529,22530,22531,23553,23554,23555,23556,24577,24578,24579,24580,25601,25602,26625,26626,27649,27650,28673,28674,28675,29697,29698,29699,29700,30721,30722,31745,31746,32769,33793,33794,34817,34818,34819,35841,35842,35843,36865,36866,37889,38913,39937,39938,40961,40962,41985,41986,41987,41988,41989,41990,43009,43010,44033,44034,45057,45058,45059,46081,46082,47105,47106,48129,48130,49153,49154,
    // Casa
    63968,64000,64032,64064,64096,64128,64160,64192,64224,64256,64288,64320,64352,64384,64416,64448,64480,64512,64544,64576,64608,64640,64672,64704,64736,64768,64800,64832,64864,64896,64928,64960,64992,65024,65056,65088,65120,65152,65184,65216,65248,65280,65312,65344,65376,65408,65440,
    // 移動局と付番ミス？
    63488,12345
]);

const rmTacSpecialMap = {
    111:"京浜 内陸",
    112:"京浜 湾岸",
    113:"城西",
    114:"埼京線~西武池袋線",
    211:"山手線 東京~大塚",
    212:"山手線 有楽町~恵比寿",
    213:"山手線 池袋~渋谷",
    214:"山手線 内側",
    311:"大宮+埼京線の東",
    312:"千葉都会",
    313:"城東の南",
    314:"城東の北",
    25511:"大阪 北",
    25512:"大阪 南",
    25513:"神戸",
    25514:"京都",
    33411:"名古屋",
    55555:"杉並世田谷",	// 多分ゴミデータ
    55556:"夢の島",		// 多分ゴミデータ
    63488:"移動局",
};
const rmTacPrefAry= [
    "？",
    "北海道",
    "青森県",
    "岩手県",
    "宮城県",
    "秋田県",
    "山形県",
    "福島県",
    "茨城県",
    "栃木県",
    "群馬県",
    "埼玉県",
    "千葉県",
    "東京都下",
    "？",
    "神奈川県",
    "新潟県",
    "富山県",
    "石川県",
    "福井県",
    "山梨県",
    "長野県",
    "岐阜県",
    "静岡県",
    "愛知県",	// 24576～25599 大阪 北 南・神戸・京都 がここに含まれる模様
    "三重県",
    "滋賀県",
    "京都府",
    "大阪府",
    "兵庫県",
    "奈良県",
    "和歌山県",
    "鳥取県",
    "島根県",   // 33411:名古屋 33793:島根県 これ以外（33600台）で静岡にあるのはミスっぽい
    "岡山県",
    "広島県",
    "山口県",
    "徳島県",   // 37889以外で熊本にあるのはミスっぽい
    "香川県",
    "愛媛県",
    "高知県",
    "福岡県",
    "？",
    "佐賀県",
    "長崎県",
    "熊本県",
    "大分県",
    "宮崎県",
    "鹿児島県",
    "沖縄県",
    ];
const rmTacFemtoPrefAry= [
    "東京都",
    "千葉県",
    "埼玉県",
    "神奈川県",
    "群馬県",
    "新潟県",
    "山梨県",
    "長野県",
    "茨城県",
    "栃木県",
    "北海道",
    "青森県",
    "岩手県",
    "宮城県",
    "秋田県",
    "山形県",
    "福島県",
    "静岡県",
    "愛知県",
    "三重県",
    "岐阜県",
    "京都府",
    "大阪府",
    "兵庫県",
    "奈良県",
    "和歌山県",
    "滋賀県",
    "富山県",
    "石川県",
    "福井県",
    "鳥取県",
    "島根県",
    "岡山県",
    "広島県",
    "山口県",
    "徳島県",
    "香川県",
    "愛媛県",
    "高知県",
    "福岡県",
    "佐賀県",
    "長崎県",
    "熊本県",
    "大分県",
    "宮崎県",
    "鹿児島県",
    "沖縄県",
    ];
const rmEnbPrefAry= [
    "？県",
    "北海道",
    "北海道？",
    "北海道？",
    "北海道？",
    "北海道？",
    "青森県",
    "秋田県",
    "岩手県",
    "宮城県",
    "山形県",
    "福島県",
    "新潟県",
    "長野県",
    "群馬県",
    "栃木県",
    "茨城県",
    "東京都",
    "東京都？",
    "千葉県",
    "神奈川県",
    "埼玉県",
    "愛知県",
    "静岡県",
    "山梨県",
    "岐阜県",
    "三重県",
    "大阪府",
    "大阪府？",
    "和歌山県",
    "奈良県",
    "京都府",
    "石川県",
    "富山県",
    "滋賀県",
    "福井県",
    "兵庫県",
    "広島県",
    "山口県",
    "島根県",
    "鳥取県",
    "岡山県",
    "香川県",
    "徳島県",
    "高知県",
    "愛媛県",
    "福岡県",
    "佐賀県",
    "長崎県",
    "熊本県",
    "大分県",
    "宮崎県",
    "沖縄県",
    "鹿児島県",
];
const rmEnbTypeAry= [
    "マクロセル",
    "Casa",
    "屋内局",
    "ピコセル",
];

function createPopupMsg(marker) {
    let msg = "";
    if ( marker.hasOwnProperty('name') ) {
        // マイマップマーカーからのポップアップ
        msg = marker.name + '<br/>' + marker.description;
    }
    else if ( marker.hasOwnProperty('cell') ) {
        // MLSマーカーからのポップアップ
        let cell = marker.cell;
        let eNB = Math.floor(cell.cell/256);
        let sector = cell.cell%256;
        let colorId = Math.floor((sector-1)/3);
        if ( 4<=colorId ) colorId = 4;

        // TAC判定
        let tacPrefType = "";
        if ( rmTacSpecialMap.hasOwnProperty(cell.area) ) {
            // 特殊扱い
            tacPrefType = rmTacSpecialMap[cell.area];
        } else if ( 63968<=cell.area ) {
            // Casa
            let tacPrefNum = Math.floor((cell.area-63968)/32);
            if ( tacPrefNum<rmTacFemtoPrefAry.length) {
                tacPrefType = rmTacFemtoPrefAry[tacPrefNum] + ',Casa';
            } else {
                tacPrefType = "Casa?";
            }
        } else {
            // 上2桁で判断
            let rmTacPrefNum = Math.floor(cell.area/1000);
            if ( rmTacPrefAry.length<=rmTacPrefNum ) rmTacPrefNum=0;
            tacPrefType = rmTacPrefAry[rmTacPrefNum];
        }

        // eNB判定
        let eNBPrefType = "";
        let eNBprefNum = Math.floor(eNB/0x4000);
        let eNBlow14bit = eNB & 0x3fff;
        if ( eNBprefNum==62 ) {
            eNBPrefType = "移動局";
        }
        else {
            // 都道府県
            if ( rmEnbPrefAry.length<=eNBprefNum ) eNBprefNum=0;
            let eNBPref = rmEnbPrefAry[eNBprefNum];

            // セルタイプ
            let eNBTypeNum = Math.floor(eNBlow14bit/5000);
            let eNBType = rmEnbTypeAry[eNBTypeNum];
            if (eNBTypeNum==0 && 4000 <= eNBlow14bit) {
            	// マクロセルの4000以降は特殊
                if (eNBlow14bit < 4600) {
                    eNBType = "ミニマクロ";
                } else {
                    eNBType = "衛星局";
                }
            }
            
            eNBPrefType = eNBPref + ',' + eNBType;
        }

        // PCI判定
        let pciType = "";
        if ( 0<=cell.unit && cell.unit<=379 ) {
            pciType = "マクロセル";
        } else if ( cell.unit<=403 ) {
            pciType = "ピコセル";
        } else if ( cell.unit<=479 ) {
            pciType = "屋内局";
        } else if ( cell.unit<=491 ) {
            pciType = "ピコセル";   // または移動局なんだけど、その旨書くとごちゃつくので省略
        } else if ( cell.unit<=503 ) {
            pciType = "Casa";
        } else {
            pciType = "LTE規格外";
        }

        let created = new Date(cell.created * 1000).toLocaleString('ja-JP');
        let updated = new Date(cell.updated * 1000).toLocaleString('ja-JP');

        msg  = eNB + '-' + sector + ' (' + eNBPrefType + ')';
        msg += '<br/>TAC:' + cell.area + ' (' + tacPrefType + ')';
        msg += '<br/>PCI:' + cell.unit + ' (' + pciType + ')';
        msg += '<br/>created:' + created;
        msg += '<br/>updated:' + updated;
        //msg += '<br/>samples:'+ cell.samples +' range:'+  cell.range;
    }
    return msg;
}