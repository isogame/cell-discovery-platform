class EciUtil {

    constructor(){
        var regEnb = "[0-9]{5,7}";
        var regLcid = "[0-9]{1,2}";
    
        // 標準
        this.regexEnbStd = new RegExp(`(${regEnb})-(${regLcid})(,${regLcid})+\\b`, 'g');
        this.regexEnbStdOne = new RegExp(`(${regEnb})-(${regLcid})\\b`, 'g');
        this.regexEnbRange = new RegExp(`(${regEnb})-(${regLcid})\\~(${regLcid})\\b`, 'g');

        // 春日部(@1mai_gear 1Tq3rUIt72m3evbHxYE6J97A6wgwrMBuV)
        // 播磨地域(@s2auxy0t4JYTKXF 1ORPFmLWxiYyXjqDOvD105rOCIGmuerYd)
        // 旧 上尾
        this.regexEnbAgo = new RegExp(`(${regEnb}) (${regLcid})(-${regLcid})*\\b`, 'g');
    }

    // eNB-LCIDの文字列を正規表現にかけたものからCellID(整数値)の配列を取得する
    convHoge(regex, value, d2) {
        var rslt = [];
        regex.lastIndex = 0;
        while(true) {
            var matchResult = regex.exec(value);
            if ( matchResult == null ) {
                return rslt;
            }
            var eNB = matchResult[1];
            var neNB256 = Number(eNB) * 256;

            var lcidstr = matchResult[0].substring(matchResult[1].length+1);
            var lcids = lcidstr.split(d2)
            for (var lcid of lcids) {
                if ( lcid.startsWith(d2) ) {
                    lcid = lcid.substr(1);
                }
                var n = parseInt(lcid);
                rslt.push(neNB256 + n);
            }
        }
    }

    // eNB-LCIDの文字列を正規表現にかけたものからCellID(整数値)の配列を取得する（範囲指定バージョン）
    convHogeRange(regex, value,) {
        var rslt = [];
        regex.lastIndex = 0;
        while(true) {
            var matchResult = regex.exec(value);
            if ( matchResult == null ) {
                return rslt;
            }
            var eNB = matchResult[1];
            var neNB256 = Number(eNB) * 256;
            
            /*
            for (var i=2; i<matchResult.length; i++ ) {
                var lcid = matchResult[2];
                var n = parseInt(lcid);
                rslt.push(neNB256 + n);
            }
            */
            // JS2HGW modified
            var minLcid = parseInt(matchResult[2]);
            var maxLcid = parseInt(matchResult[3]);
            for (var i = minLcid; i <= maxLcid; i++) {
                rslt.push(neNB256 + i);
            }
        }
    }
    
    getEciFromMemo(value) {
        var rslt = [];

        // 標準
        rslt = rslt.concat(this.convHoge(this.regexEnbStd, value, ','));
        // 春日部・播磨地域 （旧 上尾）　（１つバージョンもココ）
        rslt = rslt.concat(this.convHoge(this.regexEnbAgo, value, '-'));
        // 標準 範囲バージョン
        rslt = rslt.concat(this.convHogeRange(this.regexEnbRange, value));
        // 標準 1つバージョン
        rslt = rslt.concat(this.convHoge(this.regexEnbStdOne, value, ','));

        // 重複削除＆ソートしたものを返す。
        // （標準、標準範囲バージョンが、標準1つバージョンにもマッチしちゃって重複するため）
        return Array.from(new Set(rslt)).sort();
    }

    // ECIのリストから、eNB-LCIDの文字列を作る。
    // eciAryはソート済みであること
    getEnbLcid(eciAry) {
        var enbLcid = ""
        var lastEnb = -1
        eciAry.forEach(function(eci){
            var enb = Math.floor(eci / 256);
            var lcid = eci % 256;
            if ( enbLcid == "" ) {
                enbLcid = `${enb}-${lcid}`;     // 先頭
            }
            else if (lastEnb != enb ) {
                enbLcid += ` ${enb}-${lcid}`;  // データが変だと eNBが複数ある場合があり、その対応
            }
            else {
                enbLcid += `,${lcid}`; // 2番目以降
            }
            lastEnb = enb;
        });
        return enbLcid;
    }
}