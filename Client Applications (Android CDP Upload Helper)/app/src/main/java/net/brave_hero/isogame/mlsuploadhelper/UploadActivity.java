package net.brave_hero.isogame.mlsuploadhelper;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

//Timer
import android.os.Handler;
//Toast
import android.widget.Toast;
//Uri
import android.content.ContentResolver;
import android.net.Uri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.ArrayList;

//UPLOAD SOCKET
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.atomic.AtomicInteger;

import android.os.AsyncTask;
import android.os.Looper;

public class UploadActivity extends AppCompatActivity {
    private final AtomicInteger currentpercent = new AtomicInteger(0);
    private final AtomicInteger currentbytes = new AtomicInteger(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);


        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        TextView ProgressDebug = findViewById(R.id.textView);
        ProgressDebug.setText("Initialize...");

        //Load Preferences
        SharedPreferences sharedPreferences = getSharedPreferences("app_prefs", Context.MODE_PRIVATE);
        String UploadManagerIDstring = sharedPreferences.getString("userid", "");
        boolean imsohappySwitchState = sharedPreferences.getBoolean("imsohappy", false);  // デフォルト値としてfalseを設定
        ProgressDebug.setText(ProgressDebug.getText().toString() + "\nUpload ID: " + UploadManagerIDstring + "\nI'm so Happy?: " + String.valueOf(imsohappySwitchState));


        if (Intent.ACTION_SEND.equals(action) && type != null) {
            //if ("text/plain".equals(type)) {
            //これっているやつ？
                //handleSendText(intent); // Handle text being sent
            //}
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("text/")) {
                //handleSendMultipleImages(intent); // Handle multiple images being sent
            }
        } else {
            // Handle other intents, such as being started from the home screen
        }
        if (type != null) {
            if ("application/json".equals(type)) {
                // 単一のアイテムが共有された場合
                if (Intent.ACTION_SEND.equals(action)) {
                    Uri jsonUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
                    if (jsonUri != null) {
                        String content = readTextFromUri(jsonUri);
                        // contentを使って何かの処理を行う
                        //textView.setText(content);
                        if (UploadManagerIDstring != null && UploadManagerIDstring != "") {
                            TextView UploadProgressText = findViewById(R.id.UploadProgressText);
                            UploadProgressText.setText("アップロードしています");

                            ProgressBar uploadprogressbar = findViewById(R.id.uploadprogressbar);
                            uploadprogressbar.setMax(100);

                            byte[] totalbytescal = content.getBytes();
                            int totalBytes = totalbytescal.length;

                            final Handler handler = new Handler();
                            final Runnable updateRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    int currentPercent = currentpercent.get();
                                    int currentBytes = currentbytes.get();
                                    ProgressBar uploadprogressbar = findViewById(R.id.uploadprogressbar);
                                    uploadprogressbar.setProgress(currentPercent);
                                    TextView uploadprogresspercent = findViewById(R.id.uploadprogresspercent);
                                    uploadprogresspercent.setText(currentPercent+"%");
                                    TextView uploadprogressbytes = findViewById(R.id.uploadprogressbytes);
                                    uploadprogressbytes.setText(currentBytes+"/"+totalBytes);

                                    if (currentPercent < 100) {
                                        handler.postDelayed(this, 500); // 500msごとに進捗を確認
                                    } else {
                                        // 進捗が100%になったらタイマーを停止
                                        handler.removeCallbacks(this);
                                    }
                                }
                            };
                            handler.postDelayed(updateRunnable, 10);

                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... params) {
                                    sendPostRequest("https://44011.brave-hero.net/api/muhuploadv1.php?userid=" + UploadManagerIDstring, content, imsohappySwitchState);
                                    return null;
                                }
                            }.execute();
                        } else {
                            //Abort
                            new AlertDialog.Builder(UploadActivity.this)
                                    .setTitle("アップロードマネージャーIDが入力されていません")
                                    .setMessage("このアプリケーションを利用するには、MLSデータアップロードマネージャーのIDが必要です。MLS Upload Helperのアプリを起動して、MLSアップロードマネージャーのIDを入力して「保存」ボタンをタッチしてください。")
                                    .setPositiveButton("アップロードをキャンセル", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // OKボタンをクリックしたときの処理
                                            finish();
                                        }
                                    })
                            .show();
                        }


                    }
                }
                // 複数のアイテムが共有された場合
                else if (Intent.ACTION_SEND_MULTIPLE.equals(action)) {
                    ArrayList<Uri> jsonUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
                    if (jsonUris != null) {
                        for (Uri jsonUri : jsonUris) {
                            String content = readTextFromUri(jsonUri);
                            // contentを使って何かの処理を行う
                        }
                    }
                }
            }
        }



    }

    private String readTextFromUri(Uri uri) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            reader.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public void sendPostRequest(String targetUrl, String postData,boolean imsohappySwitchState) {
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(targetUrl);
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("charset", "utf-8");
            urlConnection.setRequestProperty("Content-Length", Integer.toString(postData.getBytes().length));

            //これで送信が可能だが、進捗表示がしたいがために以下のコードへ
//            try (OutputStream os = new BufferedOutputStream(urlConnection.getOutputStream())) {
//                os.write(postData.getBytes());
//                os.flush();
//            }

            try (OutputStream os = new BufferedOutputStream(urlConnection.getOutputStream())) {
                byte[] dataBytes = postData.getBytes();
                int totalBytes = dataBytes.length;
                int bytesUploaded = 0;
                int bufferSize = 102400; // 100KBのチャンクサイズ
                for (int i = 0; i < totalBytes; i += bufferSize) {
                    int bytesToWrite = Math.min(bufferSize, totalBytes - i);
                    os.write(dataBytes, i, bytesToWrite);
                    bytesUploaded += bytesToWrite;
                    currentpercent.set((int) ((bytesUploaded / (float) totalBytes) * 100));  // 進捗の報告
                    currentbytes.set((int) (bytesUploaded));  // 進捗の報告
                }
                os.flush();
            }

            int responseCode = urlConnection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK) {
                // 送信成功
                Handler mainThreadHandler = new Handler(Looper.getMainLooper());
                mainThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {

                        if (imsohappySwitchState == true) {
                            Toast.makeText(getApplicationContext(), "I'm so Happy", Toast.LENGTH_SHORT).show();
                        }
                         //5秒後にActivityを閉じる
                        new Handler().postDelayed(new Runnable() {
                            @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "アップロードが成功しました", Toast.LENGTH_LONG).show();
                                    finish();
                                }
                        }, 3000);  // 5000ミリ秒 = 5秒
                    }
                });
            } else {
                // 送信失敗
                Handler mainThreadHandler = new Handler(Looper.getMainLooper());
                mainThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (imsohappySwitchState == true) {
                            Toast.makeText(getApplicationContext(), "Good bye or die!", Toast.LENGTH_LONG).show();
                        }
                        //5秒後にActivityを閉じる
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "アップロードが失敗しました", Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }, 3000);  // 5000ミリ秒 = 5秒
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

//    private class UploadFileTask extends AsyncTask<Void, Integer, Boolean> {
//
//        private String targetUrl; // アップロードするファイルのパスを保存する変数
//        private String uploadcontent; // アップロードするファイルのパスを保存する変数
//        private Boolean imsohappySwitchState; // アップロードするファイルのパスを保存する変数
//
//        // カスタマイズされたコンストラクタ
//        public UploadFileTask(String targetUrl, String uploadcontent, Boolean imsohappySwitchState) {
//            this.targetUrl = targetUrl;
//            this.uploadcontent = uploadcontent;
//            this.imsohappySwitchState = imsohappySwitchState;
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... params) {
//            HttpURLConnection urlConnection = null;
//            // アップロード処理をここに書く
//            URL url = new URL(targetUrl);
//            urlConnection = (HttpURLConnection) url.openConnection();
//
//            urlConnection.setDoOutput(true);
//            urlConnection.setRequestMethod("POST");
//            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            urlConnection.setRequestProperty("charset", "utf-8");
//            urlConnection.setRequestProperty("Content-Length", Integer.toString(postData.getBytes().length));
//
//            //これで送信が可能だが、進捗表示がしたいがために以下のコードへ
////            try (OutputStream os = new BufferedOutputStream(urlConnection.getOutputStream())) {
////                os.write(postData.getBytes());
////                os.flush();
////            }
//
//            try (OutputStream os = new BufferedOutputStream(urlConnection.getOutputStream())) {
//                byte[] dataBytes = postData.getBytes();
//                int totalBytes = dataBytes.length;
//                int bytesUploaded = 0;
//                int bufferSize = 102400; // 100KBのチャンクサイズ
//                for (int i = 0; i < totalBytes; i += bufferSize) {
//                    int bytesToWrite = Math.min(bufferSize, totalBytes - i);
//                    os.write(dataBytes, i, bytesToWrite);
//                    bytesUploaded += bytesToWrite;
//                    publishProgress((int) ((bytesUploaded / (float) totalBytes) * 100));  // 進捗の報告
//                }
//                os.flush();
//            }
//
//            // 例: 進捗の計算と更新
//            int bytesUploaded = ...; // アップロードしたバイト数
//            int totalBytes = ...; // アップロードする合計バイト数
//            int progress = (int) ((bytesUploaded / (float) totalBytes) * 100);
//            publishProgress(progress);
//
//            return true; // アップロードが成功した場合
//        }
//
//        @Override
//        protected void onProgressUpdate(Integer... values) {
//            // 進捗を表示するUIコンポーネントを更新
//            int progressValue = values[0];
//            // 例: ProgressBarの更新
//            progressBar.setProgress(progressValue);
//        }
//
//        @Override
//        protected void onPostExecute(Boolean result) {
//            if (result) {
//                if (imsohappySwitchState == true) {
//                    Toast.makeText(getApplicationContext(), "I'm so Happy", Toast.LENGTH_SHORT).show();
//                }
//                //5秒後にActivityを閉じる
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), "アップロードが成功しました", Toast.LENGTH_LONG).show();
//                        finish();
//                    }
//                }, 3000);  // 5000ミリ秒 = 5秒
//            } else {
//                if (imsohappySwitchState == true) {
//                    Toast.makeText(getApplicationContext(), "Good bye or die!", Toast.LENGTH_LONG).show();
//                }
//                //5秒後にActivityを閉じる
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), "アップロードが失敗しました", Toast.LENGTH_LONG).show();
//                        finish();
//                    }
//                }, 3000);  // 5000ミリ秒 = 5秒
//            }
//        }
//    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // Update UI to reflect text being shared
            TextView textView = findViewById(R.id.textView);
            textView.setText(sharedText);
        }
    }
}