package net.brave_hero.isogame.mlsuploadhelper;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import net.brave_hero.isogame.mlsuploadhelper.databinding.FragmentFirstBinding;
//Toast
import android.widget.Toast;
//PreferencesAccess
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.EditText;
//About
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView abouttext = getView().findViewById(R.id.abouttext);
        abouttext.setMovementMethod(ScrollingMovementMethod.getInstance());

        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("app_prefs", Context.MODE_PRIVATE);
        String UploadManagerIDstring = sharedPreferences.getString("userid", "");
        EditText editText = getView().findViewById(R.id.UploadManagerID);
        editText.setText(UploadManagerIDstring);

        Switch happyswitchView = getView().findViewById(R.id.happyswitch);
        boolean savedSwitchState = sharedPreferences.getBoolean("imsohappy", false);  // デフォルト値としてfalseを設定
        happyswitchView.setChecked(savedSwitchState);

        binding.idsave.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EditText editText = getView().findViewById(R.id.UploadManagerID);
            String inputValue = editText.getText().toString();

            SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("app_prefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("userid", inputValue);
            editor.apply();

            Switch happyswitchView = getView().findViewById(R.id.happyswitch);
            boolean happyswitchState = happyswitchView.isChecked();
            editor.putBoolean("imsohappy", happyswitchState);
            editor.apply();

            Toast.makeText(getActivity().getApplicationContext(), "保存しました", Toast.LENGTH_LONG).show();
            }
        });


        binding.buttonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}