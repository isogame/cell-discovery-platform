<?php
require_once("../database_connector.php");
$db_session = dbconnect();

//mode
$mode=escs($db_session,$_POST["mode"]);

$loginid=escs($db_session,$_COOKIE["loginid"]);
//Nickname
$nickname=escs($db_session,$_POST["nickname"]);

$value = escs($db_session,$_POST["value"]);

// if ( rivalid_me < 50000000000000 || rivalid_me > 70000000000000 ) {
// 	echo "この機能を使うには、スコアツール画面で指ナビにjubeatのプレイデータを取り込む必要があります。(400)";
// 	exit();
// }
// if ( local_genuineid == "" ) {
// 	echo "リクエストを正しく処理できませんでした。指ナビに自分のプレーデータを再度取り込みなおしてください。(400)";
// 	exit();
// }

// if ( $mode !== "new_badge_define" && $badgeid == "" ) {
// 	echo "リクエストを正しく処理できませんでした。(400)";
// 	exit();
// }

// if ( $mode == "new_badge_define" && $bName == "" ) {
// 	echo "バッジに名前がつけられていません。(400)";
// 	exit();
// }

// if ( $mode == "change_badge_define" && $bParam == "" ) {
// 	echo "バッジに名前がつけられていません。(400)";
// 	exit();
// }

// if ( $mode == "change_badge_define" && $badgeid == "" ) {
// 	echo "バッジにIDがつけられていません。(400)";
// 	exit();
// }

// if ( $mode == "delete_badge_define" && $badgeid == "" ) {
// 	echo "バッジにIDがつけられていません。(400)";
// 	exit();
// }

// //ライバルIDに対するgenuineidの検証
// if (!genuineid_validation($db_session)) {
// 	echo "リクエストを正しく処理できませんでした。指ナビに自分のプレーデータを再度取り込みなおしてください。(403)";
// 	exit();
// }//Genuine IDチェック


//バッジ定義作成
if ($mode == "new_badge_define") {
	$badgeid = time();
	$sqlquery = "INSERT INTO badge_attributedata (badgeid,rivalid,bName,bCase,bParam,ModificationDate) VALUES ('".$badgeid."','".rivalid_me."','".$bName."','".$bCase."','".$bParam."','".date("Y-m-d H:i:s")."')";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	echo "バッジを新しくつくりました。";
}

//バッジ定義削除
if ($mode == "delete_badge_define") {
	$sqlquery = "DELETE FROM badge_attributedata WHERE badgeid='".$badgeid."' and rivalid='".rivalid_me."'";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	$sqlquery = "DELETE FROM badge_flagdata WHERE badgeid='".$badgeid."' and rivalid='".rivalid_me."'";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	echo "バッジをけしました。";
}

//ニックネームの設定を変更
if ($mode == "change_nickname") {
	$sqlquery = "UPDATE user_list SET nickname = '".$nickname."' WHERE userid='".$loginid."'";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	echo "success;";
}

//ゲームデータの作成
if ($mode == 'create_gamedata' ) {
	$sqlquery = "INSERT INTO badge_flagdata (badgeid,rivalid,mid,difficulty) VALUES ('".$badgeid."','".rivalid_me."','".$mid."','".$difficulty."')";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	if ( $badgeid > 1000 ) {
		$sqlquery = "UPDATE badge_attributedata SET ModificationDate = '".date("Y-m-d H:i:s")."' WHERE badgeid='".$badgeid."' and rivalid='".rivalid_me."'";
		if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	}
	echo "ゲームデータを作成しました。";
}

//ゲームデータの削除
if ($mode == "delete_gamedata" ) {
	$sqlquery = "DELETE FROM badge_flagdata WHERE badgeid='".$badgeid."' and rivalid='".rivalid_me."' and mid='".$mid."' and difficulty='".$difficulty."'";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	
	if ( $badgeid > 1000 ) {
		$sqlquery = "UPDATE badge_attributedata SET ModificationDate = '".date("Y-m-d H:i:s")."' WHERE badgeid='".$badgeid."' and rivalid='".rivalid_me."'";
		if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	}
	echo "ゲームデータを削除しました。";
}

//ジオフェンシングの追加
if ($mode == 'add_geofencing' ) {
	$geofencingdata = explode(",", escs($db_session,$_POST["value"]));
	$sqlquery = "INSERT INTO user_geofencinglist (userid,lat,lng,radius) VALUES ('".$loginid."','".$geofencingdata[0]."','".$geofencingdata[1]."','".$geofencingdata[2]."')";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	echo "success;".$geofencingdata[0].",".$geofencingdata[1]."の周辺 ".$geofencingdata[2]."m を新しくアップロード除外地域として設定しました。";
}

//ジオフェンシングの削除
if ($mode == "remove_geofencing" ) {
	$geofencingdata = explode(",", escs($db_session,$_POST["value"]));
	$sqlquery = "DELETE FROM user_geofencinglist WHERE userid='".$loginid."' and lat='".$geofencingdata[0]."' and lng='".$geofencingdata[1]."'";
	echo $sqlquery;
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	echo "success;".$geofencingdata[0].",".$geofencingdata[1]."の周辺をアップロード除外から削除しました。";
}

//アップロード除外eNB-LCIDを追加
if ($mode == 'add_excludeenb' ) {
	$excludeenbdata = explode(",", escs($db_session,$_POST["value"]));
	$CellIDdivision = explode("-", $excludeenbdata[2]);
	$CellID = $CellIDdivision[0]*256 + $CellIDdivision[1];
	$sqlquery = "INSERT INTO user_execludeenblist (userid,mcc,mnc,cellid) VALUES ('".$loginid."','".$excludeenbdata[0]."','".$excludeenbdata[1]."','".$CellID."')";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	echo "success;MCC={$excludeenbdata[0]} MNC={$excludeenbdata[1]} eNB-LCID={$excludeenbdata[2]} をアップロード除外 eNB-LCID に追加しました。";
}

//アップロード除外eNB-LCIDを削除
if ($mode == "remove_excludeenb" ) {
	$excludeenbdata = explode(",", escs($db_session,$_POST["value"]));
	$CellIDdivision = explode("-", $excludeenbdata[2]);
	$CellID = $CellIDdivision[0]*256 + $CellIDdivision[1];
	$sqlquery = "DELETE FROM user_execludeenblist WHERE userid='".$loginid."' and MCC='".$excludeenbdata[0]."' and MNC='".$excludeenbdata[1]."' and cellid='".$CellID."'";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	echo "success;MCC={$excludeenbdata[0]} MNC={$excludeenbdata[1]} eNB-LCID={$excludeenbdata[2]} をアップロード除外 eNB-LCID から削除しました。";
}

//44053はアップロードしない
if ($mode == 'exclude44053' ) {
	$sqlquery = "UPDATE user_list SET exclude44053 = '".$value."' WHERE userid='".$loginid."'";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	if ($value == "1") {
		$resultmsg = "しません。";
	} else {
		$resultmsg = "します。";
	}
	echo "success;楽天モバイル auローミングエリアの測定値をアップロード".$resultmsg.$value;
}

//44011のみアップロード
if ($mode == 'only44011' ) {
	$sqlquery = "UPDATE user_list SET only44011 = '".$value."' WHERE userid='".$loginid."'";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	if ($value == "1") {
		$resultmsg = "しません。";
	} else {
		$resultmsg = "します。";
	}
	echo "success;楽天モバイルMNO(MCC=440,MNC=11)以外のキャリア測定値をアップロード".$resultmsg;
}

//速度上限値超えた場合の動作
if ($mode == 'speedunlimit' ) {
	$sqlquery = "UPDATE user_list SET speedunlimit = '".$value."' WHERE userid='".$loginid."'";
	if (!$result = $db_session->query($sqlquery)) { echo "error"; }
	if ($value == "1") {
		$resultmsg = "します。";
	} else {
		$resultmsg = "しません。";
	}
	echo "success;特定条件に該当するケースで速度値をアップロード".$resultmsg;
}

//MySQL接続解除
dbdisconnect($db_session);
?>