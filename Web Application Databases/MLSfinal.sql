-- phpMyAdmin SQL Dump
-- version 5.2.1-1.el7.remi
-- https://www.phpmyadmin.net/
--
-- ホスト: mysql1b.minibird.netowl.jp
-- 生成日時: 2024 年 4 月 14 日 17:05
-- サーバのバージョン： 5.7.27
-- PHP のバージョン: 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `isogame_mls`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `MLSfinal`
--

CREATE TABLE `MLSfinal` (
  `radio` varchar(5) NOT NULL,
  `mcc` varchar(3) NOT NULL,
  `net` varchar(3) NOT NULL,
  `area` varchar(5) NOT NULL,
  `cell` varchar(9) NOT NULL,
  `unit` varchar(3) NOT NULL,
  `lon` double(10,7) NOT NULL,
  `lat` double(9,7) NOT NULL,
  `ranges` mediumint(6) NOT NULL,
  `samples` mediumint(5) NOT NULL,
  `changeable` tinyint(1) NOT NULL,
  `created` int(10) NOT NULL,
  `updated` int(10) NOT NULL,
  `averageSignal` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `MLSfinal`
--
ALTER TABLE `MLSfinal`
  ADD PRIMARY KEY (`mcc`,`net`,`area`,`cell`),
  ADD KEY `mcc` (`mcc`,`net`,`cell`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
