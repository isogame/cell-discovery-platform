-- phpMyAdmin SQL Dump
-- version 5.2.1-1.el7.remi
-- https://www.phpmyadmin.net/
--
-- ホスト: mysql1b.minibird.netowl.jp
-- 生成日時: 2024 年 4 月 14 日 17:04
-- サーバのバージョン： 5.7.27
-- PHP のバージョン: 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `isogame_mls`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `FullCollection`
--

CREATE TABLE `FullCollection` (
  `timestamp` varchar(13) NOT NULL,
  `latitude` double(9,7) NOT NULL,
  `longitude` double(10,7) NOT NULL,
  `accuracy` varchar(10) NOT NULL,
  `altitude` varchar(10) NOT NULL,
  `heading` varchar(10) NOT NULL,
  `speed` varchar(10) NOT NULL,
  `source` varchar(3) NOT NULL,
  `radioType` varchar(5) NOT NULL,
  `mobileCountryCode` varchar(3) NOT NULL,
  `mobileNetworkCode` varchar(3) NOT NULL,
  `locationAreaCode` varchar(6) NOT NULL,
  `cellId` varchar(11) NOT NULL,
  `primaryScramblingCode` varchar(3) NOT NULL,
  `asu` varchar(4) NOT NULL,
  `signalStrength` varchar(4) NOT NULL,
  `timingAdvance` varchar(3) NOT NULL,
  `serving` varchar(1) NOT NULL,
  `CreatedTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `FullCollection`
--
ALTER TABLE `FullCollection`
  ADD PRIMARY KEY (`timestamp`,`latitude`,`longitude`,`cellId`),
  ADD KEY `mobileCountryCode` (`mobileCountryCode`,`mobileNetworkCode`,`cellId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
