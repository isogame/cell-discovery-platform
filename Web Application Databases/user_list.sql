-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: mysql1b.minibird.netowl.jp
-- Generation Time: 2023 年 11 月 04 日 21:22
-- サーバのバージョン： 5.7.27
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isogame_mls`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `user_list`
--

CREATE TABLE IF NOT EXISTS `user_list` (
  `userid` varchar(40) NOT NULL,
  `nickname` varchar(40) NOT NULL,
  `register_date` date NOT NULL,
  `lastupdate_date` date NOT NULL,
  `game` tinyint(1) NOT NULL,
  `upload_count` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `cell` int(11) NOT NULL,
  `exclude44053` tinyint(1) NOT NULL,
  `only44011` tinyint(1) NOT NULL,
  `speedunlimit` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_list`
--
ALTER TABLE `user_list`
  ADD PRIMARY KEY (`userid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
