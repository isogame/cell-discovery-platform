-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: mysql1b.minibird.netowl.jp
-- Generation Time: 2023 年 11 月 04 日 21:23
-- サーバのバージョン： 5.7.27
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isogame_mls`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `queue_data`
--

CREATE TABLE IF NOT EXISTS `queue_data` (
  `uploadid` varchar(32) NOT NULL,
  `itemid` varchar(32) NOT NULL,
  `timestamp` varchar(13) NOT NULL,
  `latitude` double(9,7) NOT NULL,
  `longitude` double(10,7) NOT NULL,
  `accuracy` varchar(10) NOT NULL,
  `altitude` varchar(10) NOT NULL,
  `heading` varchar(10) NOT NULL,
  `speed` varchar(10) NOT NULL,
  `source` varchar(3) NOT NULL,
  `radioType` varchar(5) NOT NULL,
  `mobileCountryCode` varchar(3) NOT NULL,
  `mobileNetworkCode` varchar(3) NOT NULL,
  `locationAreaCode` varchar(6) NOT NULL,
  `cellId` varchar(9) NOT NULL,
  `primaryScramblingCode` varchar(3) NOT NULL,
  `asu` varchar(4) NOT NULL,
  `signalStrength` varchar(4) NOT NULL,
  `timingAdvance` varchar(3) NOT NULL,
  `serving` varchar(1) NOT NULL,
  `uploadedTime` datetime NOT NULL,
  `uploadCancel` tinyint(1) NOT NULL,
  `cancelReason` tinyint(4) NOT NULL,
  `NotRegisteredCell` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `queue_data`
--
ALTER TABLE `queue_data`
  ADD PRIMARY KEY (`uploadid`,`itemid`),
  ADD UNIQUE KEY `uploadid` (`uploadid`,`itemid`),
  ADD KEY `cellId` (`cellId`),
  ADD KEY `cellId_2` (`cellId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
